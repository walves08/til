# Posteriormente tentar quebrar os arquivos com python


from mdsplit import split

# Specify the path to your Markdown file
path_to_file = "path_to_your_file.md"

# Split the file using the specified separator
sections = split(path_to_file, separator="---")

# Save each section to separate files
for i, section in enumerate(sections):
    with open(f"output_section_{i}.md", "w", encoding="utf-8") as output_file:
        output_file.write(section)


