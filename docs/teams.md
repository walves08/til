---
title: Teams API
date: 2024-09-28
tags:
    - sas
    - api
    - teams
---

Enviando dados para um canal no Teams via incoming webhook

```python title="Usando PROC HTTP" linenums="1"
filename resp temp;

proc http
    url="https://banco365.webhook.office.com"
    method="POST"
    in='{"title": "Titulo mensagem", "text":"mensagem **teste** formatada."}'
    out=resp;
    headers
        "Content-Type"="application/json"
	;
run;

data _null_;
    infile resp;
    input;
    put _infile_;
run;
```

```python title="Usando CURL" linenums="1"
x curl -H 'Content-Type: application/json' -d '{"text": "Hello World"}' https://banco365.webhook.office.com ;
```
