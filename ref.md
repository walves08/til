O que é Ciência de Dados?

Começaremos esta seção dando nossa definição de CD: a CD é uma área de conhecimento que estuda e aplica princípios e técnicas, implementados por meio de algoritmos, para extrair conhecimento novo, relevante e útil de um conjunto de dados. Ela busca respostas para a seguinte pergunta: como extrair (eficientemente) conhecimento em (grandes) conjuntos (de fluxos) de dados que apoie o processo de tomada de decisão?

Com frequência, o termo CD é empregado como sinônimo de Big Data. Nosso entendimento é que enquanto Big Data diz respeito ao estudo e desenvolvimento de tecnologias para lidar com dados, englobando coleta, armazenamento, processamento e transmissão de dados, CD lida com a formulação de soluções computacionais para transformação, preprocessamento e modelagem de dados que permitam extrair conhecimento de um conjunto de dados (CARVALHO, 2024). 



Aplicações de Ciência de Dados

O número e diversidade de aplicações que têm se beneficiado do uso de técnicas de CD cresce a cada ano. Dada a grande variedade dessas aplicações, não existe uma única forma de organizá-las e representá-las. (CARVALHO, 2024)


A ciência de dados se concentra nos dados e encontra maneiras de analisá-los e apresentar dados para chegar a conclusões estatisticamente válidas (HUA, 2024). 


Em suma, a Ciência de Dados é devotada à extração de informa-
ção útil a partir de imensas, complexas e dinâmicas
bases de dados (Bugnion, Manivannan e Nicolas,
2017). Entende-se que a Ciência de Dados é um con-
ceito conexo à camada dos métodos, na qual os softwa-
res são empregados para transformar dados em infor-
mação, resultando no apoio à tomada de decisão (Rautenberg, 2019).


Aliado ao surgimento do Big Data, tem-se o advento
da Ciência de Dados como um campo de atuação de
competências interdisciplinares em ascensão. Atribui-
se à Ciência de Dados a extração de informação útil a
partir de imensas bases de dados complexas, dinâmi-
cas, heterogêneas e distribuídas (Bugnion; Manivan-
nan; Nicolas, 2017). Conforme a Figura 4, para se atuar
na Ciência de Dados, três domínios de conhecimento se
inter-relacionam: Programação de Computadores;
Estatística e Matemática; e Domínio do Conhecimento (Rautenberg, 2019).

A
Ciência de Dados se preocupa, principalmente, com o desenvolvimento e o uso de
ferramentas, bem como de processos, para extrair e discernir conhecimentos valiosos de
dados complexos (Daniel, 2020) ~(LEEK, 2013; WALLER e FAWCETT, 2013)~.


However, it is worth mentioning that, in the Brazilian case, the available public databases present problems of reliability,
unavailable data and discontinuity in the production of indicators; such factors may contribute to the low adherence to
quantitative evaluative studies. Thus, evaluations in Brazil would have more qualitative evaluations of local character and
analysis of specific policies, mainly focused on accountability and performance measurement and in search of the quality of
services provided. There are also experiments that promote interdisciplinary research and projects based on the processing,
integration, and analysis of large data quantities (big data) (Oliveira e Passador, 2019).

Contudo, vale ressaltar que, no caso brasileiro, as bases de dados públicas disponíveis apresentam problemas de confiabilidade,
indisponibilidade de dados e descontinuidade na produção de indicadores; tais fatores podem contribuir para a baixa adesão
estudos avaliativos quantitativos. Assim, as avaliações no Brasil teriam mais avaliações qualitativas de caráter local e
análise de políticas específicas, focadas principalmente na prestação de contas e medição de desempenho e na busca da qualidade do
serviços prestados. Há também experimentos que promovem pesquisas e projetos interdisciplinares baseados no processamento,
integração e análise de grandes quantidades de dados (big data).


Com o crescimento da disponibilidade de dados abertos, os órgãos pú-
blicos liberaram seus dados motivados, principalmente, pelo fato dos
dados serem financiados com orçamento público, assim trazem retorno
do investimento público, além de envolver os cidadãos através da análi-
se de grandes quantidades de conjuntos de dados.
Os órgãos públicos
estão entre os maiores produtores de dados em diversos temas como
clima, turismo, segurança e educação (SIlva e Julião, 2021) ~(JANSSEN; CHARALABIDIS;
ZUIDERWIJK, 2012)~.


Acredita-se que, no caso brasileiro, as bases de dados públicas disponíveis apresentam problemas de confiabilidade (algumas apresentam dados declarados), dados indisponíveis e descontinuidade na produção de indicadores; tais fatores podem contribuir com a baixa adesão a estudos avaliativos quantitativos. Assim, as avaliações no país estariam entre a segunda onda proposta por Vedung (2010), com maior quantidade de avaliações qualitativas de caráter local e análise de políticas especificas, e a terceira onda, voltada principalmente a prestação de contas, medição de desempenho e busca da qualidade dos serviços prestados (Oliveira e Passador, 2019).






CARVALHO, André C. P. L. F de; MENEZES, Angelo G.; BONIDIA, Robson P. Ciência de Dados - Fundamentos e Aplicações. Rio de Janeiro: Grupo GEN, 2024. E-book. ISBN 9788521638766. Disponível em: https://integrada.minhabiblioteca.com.br/#/books/9788521638766/. Acesso em: 01 set. 2024.

HUA, Chew C. Inteligência Artificial, Análise e Ciência de Dados: Conceitos fundamentais e modelos. São Paulo: Cengage Learning Brasil, 2024. E-book. ISBN 9786555584653. Disponível em: https://integrada.minhabiblioteca.com.br/#/books/9786555584653/. Acesso em: 01 set. 2024.

Rautenberg, Sandro, e Paulo Ricardo Viviurka do Carmo. “Big Data E Ciência De Dados: Complementariedade Conceitual No Processo De Tomada De decisão”. Brazilian Journal of Information Science: Research Trends, vol. 13, nº 1, março de 2019, p. 56-67, https://doi.org/10.36311/1981-1640.2019.v13n1.06.p56.


Ciência de Dados na Administração Pública: Desafios e Oportunidades. Revista da CGU, [S. l.], v. 14, n. 26, 2022. DOI: 10.36428/revistadacgu.v14i26.617. Disponível em: https://revista.cgu.gov.br/Revista_da_CGU/article/view/617.. Acesso em: 1 set. 2024.

 Como Citar
DANIEL, Ben Kei; MAIA, Tradução: Mirtes Dâmares Santos de Almeida; SILVA, Danilo Garcia da. Big Data e ciência de dados: uma revisão crítica de questões para a pesquisa educacional. PerCursos, Florianópolis, v. 21, n. 45, p. 80–103, 2020. DOI: 10.5965/1984724621452020080. Disponível em: https://periodicos.udesc.br/index.php/percursos/article/view/1984724621452020080. Acesso em: 1 set. 2024.


OLIVEIRA, L. R. DE .; PASSADOR, C. S.. Ensaio teórico sobre as avaliações de políticas públicas. Cadernos EBAPE.BR, v. 17, n. 2, p. 324–337, abr. 2019. 
https://www.scielo.br/j/cebape/a/svZxsKnLTZ4RWnLGG93bYfH/?lang=pt#


R A FA E L LO P E S DA S I LVA, R U I P E D R O  J U L I ÃO
Bahia Análise & Dados / Superintendência de Estudos
Econômicos e Sociais da Bahia. v. 1 (1991 - ).
Salvador : SEI, 2021.
v. 31
n. 2
Semestral
ISSN 0103-8117
EISSN 2595-2064
Salvador, v. 31, n. 2,
p.54-76, jul.-dez. 2021
https://www.sei.ba.gov.br/images/publicacoes/download/aed/ciencia_tecnologia_inov.pdf


LEI Nº 13.460, DE 26 DE JUNHO DE 2017.
Dispõe sobre participação, proteção e defesa dos direitos do usuário dos serviços públicos da administração pública.